package com.zuitt.WDC043_S03;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<>();

    // constructor
    public Phonebook() {}

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // method to add a new contact
    public void addContact(Contact newContact) {
        contacts.add(newContact);
    }

    // method to get all contacts
    public ArrayList<Contact> getAllContacts() {
        return contacts;
    }
}
