import com.zuitt.WDC043_S03.Contact;
import com.zuitt.WDC043_S03.Phonebook;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596","+639228547963", "Quezon City","Makati City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573","+639173698541", "Caloocan City","Pasay City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if(phonebook.getAllContacts().size()>0) {
            for (Contact contact : phonebook.getAllContacts()) {
                System.out.println(contact.getName());
                System.out.println("----------------");
                System.out.println(contact.getName() + " has the following registered numbers:");

                System.out.println(contact.getContactNumber1());
                System.out.println(contact.getContactNumber2());

                System.out.println("----------------");
                System.out.println(contact.getName() + " has the following registered addresses:");

                System.out.println("my home in "+ contact.getAddress1());
                System.out.println("my office in "+ contact.getAddress2());
                System.out.println("===================================");
            }
        }else{
            System.out.println("Your Phonebook is empty!");
        }

    }
}